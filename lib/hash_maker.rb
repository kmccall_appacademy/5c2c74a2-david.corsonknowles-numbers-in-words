class Fixnum

  def in_words
    result = ""
    string = self.to_s
    if string.length == 2 && self < 20
      return teens(self)
    end
    if string.length == 1
      return ones_digit(self)
    end

    if string.length == 4
      result << ones_digit(string[-4].to_i) + " thousand"
      string = string[-3..-1]
      if string == "000"
        return result
      else
        result << " "
      end
    end

    if string.length == 3
      result << hundreds(string[-3].to_i * 100)
      string = string[-2..-1]
      if string != "00"
        result << " "
      end
    end

    if string.length == 2 && string.to_i > 19
      result << tens_digit(string[-2].to_i * 10)
      result << " " + ones_digit(string[-1].to_i) if string[-1].to_i != 0
    elsif string.length == 2 && string.to_i < 20 && string.to_i > 9
      result << teens(string.to_i)
    else
    end

    result
  end

  def ones_digit(n)
    keys = %w(0 1 2 3 4 5 6 7 8 9)
    values = %w(zero one two three four five six seven eight nine)
    hash = keys.zip(values).to_h
    hash[n.to_s]
  end

  def teens(n)
    keys = %w(10 11 12 13 14 15 16 17 18 19)
    values = %w(ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
    hash = keys.zip(values).to_h
    hash[n.to_s]
  end

  def tens_digit(n)
    keys = %w(20 30 40 50 60 70 80 90)
    values = %w(twenty thirty forty fifty sixty seventy eighty ninety)
    hash = keys.zip(values).to_h
    hash[n.to_s]
  end

  def hundreds(n)
    keys = %w(100 200 300 400 500 600 700 800 900)
    values = ones_digit( n / 100 ) + " hundred"
  end

end

def make_hash(num)
  result = {}
  (1..num).each {|n| result[n] = n.in_words }
  result
end

p make_hash(999)
